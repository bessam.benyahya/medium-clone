import { GetStaticPaths, GetStaticProps } from "next";
import Head from "next/head";
import React, { useState } from "react";
import Header from "../../components/Header";

import { sanityClient, urlFor } from "../../sanity";
import { PostTS } from "../../typing";

import PortableText from "react-portable-text";
import Footer from "../../components/Footer";

import { useForm, SubmitHandler } from "react-hook-form";

interface IFormInput {
  _id: string;
  name: string;
  email: string;
  comment: string;
}

interface Props {
  post: PostTS;
}

export default function Post({ post }: Props) {
  const [submitted, setSubmitted] = useState(false);

  console.log("post", post);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    fetch("/api/createComment", {
      method: "POST",
      body: JSON.stringify(data),
    })
      .then(() => {
        console.log(data);
        setSubmitted(true);
      })
      .catch((error) => {
        console.log(error);
        setSubmitted(false);
      });
  };

  return (
    <div>
      <Head>
        <title>{post.title} - Binov.com</title>
        <link rel="icon" href="/binov.ico" />
      </Head>
      <Header />
      <main>
        <img
          className="w-full h-80 object-cover"
          src={urlFor(post.mainImage).url()!}
          alt={post.title}
        />

        <article className="max-w-3xl mx-auto p-5">
          <h1 className="text-3xl mt-10 mb-3">{post.title}</h1>
          {/* <h2 className="text-xl font-light text-gray-500 mb-2">
            {post.description}
          </h2> */}
          <div className="flex items-center space-x-2">
            <img
              className="h-10 w-10 rounded-full"
              src={urlFor(post.author.image).url()!}
              alt={post.author.name}
            />
            <p className="font-extralight text-sm">
              Publié par{" "}
              <span className="text-blue-400">{post.author.name}</span> - Publié
              le {new Date(post._createdAt).toLocaleString("fr")}
            </p>
          </div>

          <div>
            <PortableText
              className="pt-10"
              dataset={process.env.NEXT_PUBLIC_SANITY_DATASET!}
              projectId={process.env.NEXT_PUBLIC_SANITY_PROJECT_ID!}
              content={post.body}
              serializers={{
                h3: (props: any) => (
                  <h3 className="text-2xl font-bold my-5" {...props} />
                ),
                h4: (props: any) => (
                  <h4 className="text-xl font-bold my-5" {...props} />
                ),
                li: ({ children }: any) => (
                  <li className="ml-4 mb-2 list-disc">{children}</li>
                ),
                link: ({ href, children }: any) => (
                  <a href={href} className="text-blue-500 hover:underline">
                    {children}
                  </a>
                ),
                normal: ({ children }: any) => (
                  <p className="mb-5">{children}</p>
                ),
              }}
            />
          </div>
        </article>
        <hr className="max-w-lg my-5 mx-auto border border-blue-500" />
        {!submitted ? (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col p-5 max-w-2xl mx-auto mb-10"
          >
            <h3 className="text-sm text-blue-500">
              Vous avez aimez cet article ?
            </h3>
            <h4 className="text-3xl font-bold">Laissez un commentaire</h4>
            <hr className="py-3 mt-2" />

            <input
              {...register("_id")}
              type="hidden"
              name="_id"
              value={post._id}
            />
            <label className="block mb-5">
              <span className="text-gray-700">Prénom</span>
              <input
                {...register("name", { required: true })}
                className="shadow border rounded py-2 px-3 mt-1 block w-full ring-blue-500 form-input outline-none focus:ring"
                type="text"
                placeholder="Bessam Ben Yahia"
              />
            </label>
            <label className="block mb-5">
              <span className="text-gray-700">Email</span>
              <input
                {...register("email", { required: true })}
                className="shadow border rounded py-2 px-3 mt-1 block w-full ring-blue-500 form-input outline-none focus:ring"
                type="email"
                placeholder="bessam@binov.com"
              />
            </label>
            <label className="block mb-5">
              <span className="text-gray-700">Commentaire</span>
              <textarea
                {...register("comment", { required: true })}
                className="shadow border rounded py-2 px-3 mt-1 block w-full ring-blue-500 form-textarea outline-none focus:ring"
                placeholder="Mon commentaire..."
                rows={6}
              />
            </label>

            <div className="flex flex-col p-5">
              {errors.name && (
                <span className="text-red-500">* Nom Obligatoire</span>
              )}
              {errors.email && (
                <span className="text-red-500">* Email Obligatoire</span>
              )}
              {errors.comment && (
                <span className="text-red-500">* Commentaire Obligatoire</span>
              )}
            </div>

            <input
              className="shadow bg-blue-500 hover:bg-blue-400 focus:outline-none text-white font-bold py-2 px-4 rounded cursor-pointer"
              type="submit"
            />
          </form>
        ) : (
          <div className="flex flex-col p-10 mb-10 bg-blue-500 text-white max-w-2xl mx-auto">
            <h3 className="text-3xl font-bold">
              Merci pour votre commentaire !
            </h3>
            <p>
              Une fois approuvé, votre commentaire sera disponible ci-dessous.
            </p>
          </div>
        )}

        <div className="flex flex-col p-10 my-10 max-w-2xl mx-auto shadow shadow-blue-500 space-y-2">
          <h3 className="text-4xl">Commentaires</h3>
          <hr className="pb-2" />
          {post.comments.map((comment) => (
            <div key={comment._id}>
              <p>
                <span className="text-blue-500">{comment.name} :</span>{" "}
                {comment.comment}
              </p>
            </div>
          ))}
        </div>
      </main>
      <Footer />
    </div>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  const query = `*[_type == "post"]{ _id, slug {current}}`;

  const posts = await sanityClient.fetch(query);

  const paths = posts.map((post: PostTS) => ({
    params: { slug: post.slug.current },
  }));

  return {
    paths,
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const query = `*[_type == "post" &&  slug.current == $slug][0]{
    _id, slug, _createdAt, title, description, mainImage, body,
    author -> {
      name, 
      image
    },
    'comments': *[_type=="comment" && 
      post._ref == ^._id && 
      approved == true ]
    }`;

  const post = await sanityClient.fetch(query, { slug: params?.slug });

  if (!post) return { notFound: true };

  return {
    props: { post },
    revalidate: 60,
  };
};
