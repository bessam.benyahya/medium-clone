import Head from "next/head";
import Banner from "../components/Banner";
import Header from "../components/Header";
import Posts from "../components/Posts";

import { sanityClient } from "../sanity";
import { PostTS } from "../typing";

interface Props {
  posts: [PostTS];
}

export default function Home({ posts }: Props) {
  return (
    <div className="max-w-7xl mx-auto">
      <Head>
        <title>Binov.com : Développez avec Next.js</title>
        <link rel="icon" href="/binov.ico" />
      </Head>

      <Header />
      <main>
        <Banner />
        <Posts posts={posts} />
      </main>
    </div>
  );
}

export const getServerSideProps = async () => {
  const query = `*[_type == "post"]{
    _id,
    _createdAt,
    slug,
    title,
    author -> {
      name, 
      image
    },
    description,
    mainImage
  }`;

  const posts = await sanityClient.fetch(query);

  return {
    props: {
      posts,
    },
  };
};
