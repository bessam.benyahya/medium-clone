export interface PostTS {
  _id: string;
  _createdAt: string;
  title: string;
  description: string;
  mainImage: {
    asset: {
      url: string;
    };
  };
  slug: { current: string };
  body: [object];
  author: AuthorTS;
  comments: CommentTS[];
}

export interface AuthorTS {
  name: string;
  image: string;
}

export interface CommentTS {
  approved: boolean;
  comment: string;
  email: string;
  name: string;
  post: {
    _ref: string;
    _type: string;
  };
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: string;
  _updatedAt: string;
}
