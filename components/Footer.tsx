import React from "react";

function Footer() {
  return (
    <footer className="flex p-5 max-w-7xl mx-auto bg-blue-500 justify-center">
      <p className="text-white">
        2022 © <span className="font-semibold">Binov</span> - Tous Droits
        Réservés
      </p>
    </footer>
  );
}

export default Footer;
