import Link from "next/link";
import React from "react";

function Header() {
  return (
    <header className="flex justify-between p-5 max-w-7xl mx-auto">
      <div className="flex items-center space-x-5">
        <Link href="/">
          <img
            className="w-44 object-contain cursor-pointer"
            src="https://binov.com/wp-content/uploads/2020/07/Agence-de-Communicatio-Agence-Web-Binov-223x59-1.png"
          />
        </Link>
        <div className="hidden md:inline-flex items-center space-x-5">
          <h3>A Propos</h3>
          <h3>Contact</h3>
          <h3 className="text-white bg-blue-400 px-4 py-1 rounded-full">
            Suivez-Nous
          </h3>
        </div>
      </div>
      <div className="flex items-center space-x-5 text-blue-400">
        <h3>Login</h3>
        <h3 className="border px-4 py-1 rounded-full">S'inscrire</h3>
      </div>
    </header>
  );
}

export default Header;
