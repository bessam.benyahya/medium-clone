import React from "react";

export default function Banner() {
  return (
    <div className="flex justify-between items-center bg-gray-100 border-y border-gray-200 py-10 lg:py-0">
      <div className="px-10 space-y-5">
        <h1 className="text-6xl max-w-xl font-serif">
          <span className="underline decoration-black decoration-4">
            Développez
          </span>{" "}
          avec le Framework NextJS
        </h1>
        <h2>
          Chez Binov, nous aimons développer avec Next.js et autres technologies
          comme React.js, Node.js, Tailwind CSS, Firebase, Sanity.io, etc.
        </h2>
      </div>
      <img
        className="hidden md:inline-flex md:h-44 lg:h-full"
        src="https://binov.com/wp-content/uploads/2021/10/Agence-de-Communication-Agence-Web-Binov-ico.png"
        alt="Binov Picto"
      />
    </div>
  );
}
